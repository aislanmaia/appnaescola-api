defmodule Api.Repo do
  use Ecto.Repo, otp_app: :api
  use Scrivener, page_size: 15
end
