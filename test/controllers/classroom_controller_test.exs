defmodule Api.ClassroomControllerTest do
  use Api.ConnCase

  alias Api.Classroom
  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, classroom_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    classroom = Repo.insert! %Classroom{}
    conn = get conn, classroom_path(conn, :show, classroom)
    assert json_response(conn, 200)["data"] == %{"id" => classroom.id,
      "name" => classroom.name}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, classroom_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, classroom_path(conn, :create), classroom: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Classroom, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, classroom_path(conn, :create), classroom: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    classroom = Repo.insert! %Classroom{}
    conn = put conn, classroom_path(conn, :update, classroom), classroom: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Classroom, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    classroom = Repo.insert! %Classroom{}
    conn = put conn, classroom_path(conn, :update, classroom), classroom: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    classroom = Repo.insert! %Classroom{}
    conn = delete conn, classroom_path(conn, :delete, classroom)
    assert response(conn, 204)
    refute Repo.get(Classroom, classroom.id)
  end
end
