defmodule Api.ResponsibleControllerTest do
  use Api.ConnCase

  alias Api.Responsible
  @valid_attrs %{cpf: "some content", email: "some content", kinship: "some content", name: "some content", phone: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, responsible_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    responsible = Repo.insert! %Responsible{}
    conn = get conn, responsible_path(conn, :show, responsible)
    assert json_response(conn, 200)["data"] == %{"id" => responsible.id,
      "name" => responsible.name,
      "kinship" => responsible.kinship,
      "phone" => responsible.phone,
      "cpf" => responsible.cpf,
      "email" => responsible.email}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, responsible_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, responsible_path(conn, :create), responsible: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Responsible, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, responsible_path(conn, :create), responsible: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    responsible = Repo.insert! %Responsible{}
    conn = put conn, responsible_path(conn, :update, responsible), responsible: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Responsible, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    responsible = Repo.insert! %Responsible{}
    conn = put conn, responsible_path(conn, :update, responsible), responsible: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    responsible = Repo.insert! %Responsible{}
    conn = delete conn, responsible_path(conn, :delete, responsible)
    assert response(conn, 204)
    refute Repo.get(Responsible, responsible.id)
  end
end
