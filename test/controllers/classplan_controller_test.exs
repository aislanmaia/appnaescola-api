defmodule Api.ClassplanControllerTest do
  use Api.ConnCase

  alias Api.Classplan
  @valid_attrs %{begin_date: %{day: 17, month: 4, year: 2010}, content_topics: %{}, end_date: %{day: 17, month: 4, year: 2010}}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, classplan_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    classplan = Repo.insert! %Classplan{}
    conn = get conn, classplan_path(conn, :show, classplan)
    assert json_response(conn, 200)["data"] == %{"id" => classplan.id,
      "content_topics" => classplan.content_topics,
      "begin_date" => classplan.begin_date,
      "end_date" => classplan.end_date,
      "class_subject_id" => classplan.class_subject_id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, classplan_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, classplan_path(conn, :create), classplan: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Classplan, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, classplan_path(conn, :create), classplan: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    classplan = Repo.insert! %Classplan{}
    conn = put conn, classplan_path(conn, :update, classplan), classplan: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Classplan, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    classplan = Repo.insert! %Classplan{}
    conn = put conn, classplan_path(conn, :update, classplan), classplan: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    classplan = Repo.insert! %Classplan{}
    conn = delete conn, classplan_path(conn, :delete, classplan)
    assert response(conn, 204)
    refute Repo.get(Classplan, classplan.id)
  end
end
