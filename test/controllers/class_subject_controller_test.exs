defmodule Api.ClassSubjectControllerTest do
  use Api.ConnCase

  alias Api.ClassSubject
  @valid_attrs %{}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, class_subject_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    class_subject = Repo.insert! %ClassSubject{}
    conn = get conn, class_subject_path(conn, :show, class_subject)
    assert json_response(conn, 200)["data"] == %{"id" => class_subject.id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, class_subject_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, class_subject_path(conn, :create), class_subject: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(ClassSubject, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, class_subject_path(conn, :create), class_subject: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    class_subject = Repo.insert! %ClassSubject{}
    conn = put conn, class_subject_path(conn, :update, class_subject), class_subject: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(ClassSubject, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    class_subject = Repo.insert! %ClassSubject{}
    conn = put conn, class_subject_path(conn, :update, class_subject), class_subject: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    class_subject = Repo.insert! %ClassSubject{}
    conn = delete conn, class_subject_path(conn, :delete, class_subject)
    assert response(conn, 204)
    refute Repo.get(ClassSubject, class_subject.id)
  end
end
