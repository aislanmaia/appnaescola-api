defmodule Api.ClassControllerTest do
  use Api.ConnCase

  alias Api.Class
  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, class_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    class = Repo.insert! %Class{}
    conn = get conn, class_path(conn, :show, class)
    assert json_response(conn, 200)["data"] == %{"id" => class.id,
      "name" => class.name,
      "classroom_id" => class.classroom_id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, class_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, class_path(conn, :create), class: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Class, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, class_path(conn, :create), class: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    class = Repo.insert! %Class{}
    conn = put conn, class_path(conn, :update, class), class: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Class, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    class = Repo.insert! %Class{}
    conn = put conn, class_path(conn, :update, class), class: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    class = Repo.insert! %Class{}
    conn = delete conn, class_path(conn, :delete, class)
    assert response(conn, 204)
    refute Repo.get(Class, class.id)
  end
end
