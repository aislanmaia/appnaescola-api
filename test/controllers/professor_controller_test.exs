defmodule Api.ProfessorControllerTest do
  use Api.ConnCase

  alias Api.Professor
  @valid_attrs %{birthdate: %{day: 17, month: 4, year: 2010}, cpf: "some content", email: "some content", name: "some content", phone: 42, sex: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, professor_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    professor = Repo.insert! %Professor{}
    conn = get conn, professor_path(conn, :show, professor)
    assert json_response(conn, 200)["data"] == %{"id" => professor.id,
      "name" => professor.name,
      "birthdate" => professor.birthdate,
      "phone" => professor.phone,
      "email" => professor.email,
      "cpf" => professor.cpf,
      "sex" => professor.sex}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, professor_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, professor_path(conn, :create), professor: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Professor, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, professor_path(conn, :create), professor: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    professor = Repo.insert! %Professor{}
    conn = put conn, professor_path(conn, :update, professor), professor: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Professor, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    professor = Repo.insert! %Professor{}
    conn = put conn, professor_path(conn, :update, professor), professor: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    professor = Repo.insert! %Professor{}
    conn = delete conn, professor_path(conn, :delete, professor)
    assert response(conn, 204)
    refute Repo.get(Professor, professor.id)
  end
end
