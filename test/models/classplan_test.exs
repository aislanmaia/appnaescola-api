defmodule Api.ClassplanTest do
  use Api.ModelCase

  alias Api.Classplan

  @valid_attrs %{begin_date: %{day: 17, month: 4, year: 2010}, content_topics: %{}, end_date: %{day: 17, month: 4, year: 2010}}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Classplan.changeset(%Classplan{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Classplan.changeset(%Classplan{}, @invalid_attrs)
    refute changeset.valid?
  end
end
