defmodule Api.ResponsibleTest do
  use Api.ModelCase

  alias Api.Responsible

  @valid_attrs %{cpf: "some content", email: "some content", kinship: "some content", name: "some content", phone: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Responsible.changeset(%Responsible{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Responsible.changeset(%Responsible{}, @invalid_attrs)
    refute changeset.valid?
  end
end
