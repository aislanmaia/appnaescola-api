defmodule Api.ProfessorTest do
  use Api.ModelCase

  alias Api.Professor

  @valid_attrs %{birthdate: %{day: 17, month: 4, year: 2010}, cpf: "some content", email: "some content", name: "some content", phone: 42, sex: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Professor.changeset(%Professor{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Professor.changeset(%Professor{}, @invalid_attrs)
    refute changeset.valid?
  end
end
