defmodule Api.ClassSubjectTest do
  use Api.ModelCase

  alias Api.ClassSubject

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ClassSubject.changeset(%ClassSubject{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ClassSubject.changeset(%ClassSubject{}, @invalid_attrs)
    refute changeset.valid?
  end
end
