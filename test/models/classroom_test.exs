defmodule Api.ClassroomTest do
  use Api.ModelCase

  alias Api.Classroom

  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Classroom.changeset(%Classroom{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Classroom.changeset(%Classroom{}, @invalid_attrs)
    refute changeset.valid?
  end
end
