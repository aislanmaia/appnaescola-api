defmodule Api.StudentTest do
  use Api.ModelCase

  alias Api.Student

  @valid_attrs %{birthdate: %{day: 17, month: 4, year: 2010}, email: "some content", name: "some content", phone: "some content", sex: "some content", year: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Student.changeset(%Student{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Student.changeset(%Student{}, @invalid_attrs)
    refute changeset.valid?
  end
end
