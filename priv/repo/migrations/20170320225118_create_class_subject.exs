defmodule Api.Repo.Migrations.CreateClassSubject do
  use Ecto.Migration

  def change do
    create table(:class_subjects) do
      add :class_id, references(:classes, on_delete: :delete_all)
      add :subject_id, references(:subjects, on_delete: :delete_all)
      add :professor_id, references(:professors, on_delete: :nilify_all)

      timestamps()
    end
    create index(:class_subjects, [:class_id])
    create index(:class_subjects, [:subject_id])
    create index(:class_subjects, [:professor_id])

  end
end
