defmodule Api.Repo.Migrations.CreateProfessor do
  use Ecto.Migration

  def change do
    create table(:professors) do
      add :name, :string
      add :birthdate, :date
      add :phone, :string
      add :email, :string
      add :cpf, :string, null: false
      add :sex, :string

      timestamps()
    end
    create unique_index(:professors, [:cpf])

  end
end
