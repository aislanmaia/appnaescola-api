defmodule Api.Repo.Migrations.CreateResponsible do
  use Ecto.Migration

  def change do
    create table(:responsibles) do
      add :name, :string
      add :kinship, :string
      add :phone, :string
      add :cpf, :string
      add :email, :string

      timestamps()
    end
    create index(:responsibles, [:id])
    create unique_index(:responsibles, [:cpf])

  end
end
