defmodule Api.Repo.Migrations.CreateClass do
  use Ecto.Migration

  def change do
    create table(:classes) do
      add :name, :string
      add :classroom_id, references(:classes, on_delete: :nothing)

      timestamps()
    end
    create index(:classes, [:classroom_id])

  end
end
