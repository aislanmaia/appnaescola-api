defmodule Api.Repo.Migrations.CreateSubject do
  use Ecto.Migration

  def change do
    create table(:subjects) do
      add :name, :string, null: false
      add :optional, :boolean, default: false, null: false

      timestamps()
    end
    create unique_index(:subjects, [:name])
  end
end
