defmodule Api.Repo.Migrations.AddFixedClassroomIdToClasses do
  use Ecto.Migration

  def change do
    alter table(:classes) do
      add :classroom_id, references(:classrooms, on_delete: :nothing)
    end
  end
end
