defmodule Api.Repo.Migrations.CreateProfessorsClassesTable do
  use Ecto.Migration

  def change do
    create table(:professors_classes) do
      add :professor_id, references(:professors)
      add :class_id, references(:classes)

      timestamps()
    end

    create unique_index(:professors_classes, [:professor_id, :class_id], name: "professor_class_unique")
  end
end
