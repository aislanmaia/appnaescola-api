defmodule Api.Repo.Migrations.CreateResponsability do
  use Ecto.Migration

  def change do
    create table(:responsabilities) do
      add :responsible_id, references(:responsibles, on_delete: :delete_all)
      add :student_id, references(:students, on_delete: :delete_all)

      timestamps()
    end

    create unique_index(:responsabilities, [:responsible_id, :student_id], name: "responsible_student_unique")
  end
end
