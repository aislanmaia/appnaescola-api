defmodule Api.Repo.Migrations.AddClassIdToStudents do
  use Ecto.Migration

  def change do
    alter table(:students) do
      add :class_id, references(:classes, on_delete: :nilify_all)
    end
  end
end
