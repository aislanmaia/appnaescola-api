defmodule Api.Repo.Migrations.AddUserIdToProfessors do
  use Ecto.Migration

  def change do
    alter table(:professors) do
      add :user_id, references(:users)
    end

    create index(:professors, [:user_id])
  end
end
