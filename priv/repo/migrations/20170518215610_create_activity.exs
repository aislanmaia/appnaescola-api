defmodule Api.Repo.Migrations.CreateActivity do
  use Ecto.Migration

  def change do
    create table(:activities) do
      add :name, :string
      add :description, :string
      add :due_date, :date
      add :archive, :string

      timestamps()
    end
    create index(:activities, [:id])

  end
end
