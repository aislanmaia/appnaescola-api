defmodule Api.Repo.Migrations.CreateClassplan do
  use Ecto.Migration

  def change do
    create table(:classplans) do
      add :content_topics, {:array, :map}, default: []
      add :begin_date, :date
      add :end_date, :date
      add :attachment, :string
      add :class_subject_id, references(:class_subjects, on_delete: :nothing)
      add :uuid, :string

      timestamps()
    end
    create index(:classplans, [:class_subject_id])

  end
end
