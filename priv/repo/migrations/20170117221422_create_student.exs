defmodule Api.Repo.Migrations.CreateStudent do
  use Ecto.Migration

  def change do
    create table(:students) do
      add :name, :string
      add :birthdate, :date
      add :phone, :string
      add :email, :string
      add :year, :string
      add :sex, :string

      timestamps()
    end
    create index(:students, [:id])

  end
end
