defmodule Api.Repo.Migrations.RenameActivityArchiveColumnToFile do
  use Ecto.Migration

  def change do
    rename table(:activities), :archive, to: :file
  end
end
