defmodule Api.Repo.Migrations.RemoveYearFromStudent do
  use Ecto.Migration

  def change do
    alter table(:students) do
      remove :year
    end
  end
end
