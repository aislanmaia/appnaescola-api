defmodule Api.Repo.Migrations.AddUuidToActivities do
  use Ecto.Migration

  def change do
    alter table(:activities) do
      add :uuid, :string
    end
  end
end
