defmodule Api.Repo.Migrations.AddAdminFlagToRoles do
  use Ecto.Migration

  def change do
    alter table(:roles) do
      add :admin, :boolean, null: false, default: false
    end
  end
end
