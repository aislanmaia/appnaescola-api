defmodule Api.Repo.Migrations.RemoveClassroomIdFromClasses do
  use Ecto.Migration

  def change do
    alter table(:classes) do
      remove :classroom_id
    end
  end
end
