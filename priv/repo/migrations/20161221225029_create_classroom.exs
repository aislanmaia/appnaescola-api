defmodule Api.Repo.Migrations.CreateClassroom do
  use Ecto.Migration

  def change do
    create table(:classrooms) do
      add :name, :string, null: false

      timestamps()
    end
    create unique_index(:classrooms, [:name])
  end
end
