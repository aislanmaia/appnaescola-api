# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Api.Repo.insert!(%Api.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Api.{User, Classroom, ClassSubject, Repo, Role}

# Repo.insert! %User{
#   username: "admin",
#   email: "admin@email.com",
#   encrypted_password: Comeonin.Bcrypt.hashpwsalt("123456")
# }

defmodule MiscHelpers do
  def createClassrooms(times) do
    if times >= 1 do
      Repo.insert! %Classroom{
        name: "Sala #{times}"
      }
      createClassrooms(times - 1)
    end
  end

  def createClassSubjects(class_id, subject_id, professor_id) do
    Repo.insert! %ClassSubject{
      class_id: class_id,
      subject_id: subject_id,
      professor_id: professor_id
    }
  end

  def createRole(name, is_admin \\ false) do
    unless Repo.get_by(Role, name: name) do
      role = %Role{}
      |> Role.changeset(%{name: name, admin: is_admin})
      |> Repo.insert!
    end
  end

  def createAdminUser() do
    admin_role = Repo.get_by(Role, admin: true, name: "Admin")
    admin_params = %{username: "admin",
                     email: "admin@email.com",
                     password: "123456",
                     password_confirmation: "123456",
                     role_id: admin_role.id
                    }
    unless Repo.get_by(User, email: admin_params[:email]) do
      %User{}
      |> User.registration_changeset(admin_params)
      |> Repo.insert!
    end
  end

end


# MiscHelpers.createClassrooms(200)
# MiscHelpers.createClassSubjects(56, 1, 2)
# MiscHelpers.createRole(name: "Admin Role", admin: true)
MiscHelpers.createRole("Admin", true)
MiscHelpers.createRole("Professor")
MiscHelpers.createAdminUser()
