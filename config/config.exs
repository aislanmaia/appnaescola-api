# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :api,
  ecto_repos: [Api.Repo]

# Configures the endpoint
config :api, Api.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "dsk2nMqP5wCrKeK+zyqqxmKxkrqXtQgoujOC7g//7HU1Xv1JMVodxo4K4KbHxzOG",
  render_errors: [view: Api.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Api.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures Ueberauth
config :ueberauth, Ueberauth,
  providers: [
    identity: {
      Ueberauth.Strategy.Identity,
      [callback_methods: ["POST"]]
    }
  ]

# Configures Guardian
config :guardian, Guardian,
  issuer: "Api",
    ttl: {30, :days},
  secret_key: "Y+LEL7lgWvaVYuI/s2adhM4eTNXYar0DdwaATzPK1+dZ2Bzix5Ncht5O3i0Edfx",
    serializer: Api.GuardianSerializer,
    permissions: %{default: [:read, :write]}

# Configures Arc (to uploads)
config :arc,
  storage: Arc.Storage.S3,
  bucket: System.get_env("AWS_S3_BUCKET_UPLOADS"),
  version_timeout: 60_000, #milliseconds
  virtual_host: true

# Configures EX_Aws (to S3 uploads)
config :ex_aws,
  access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, :instance_role],
  secret_access_key: [{:system, "AWS_SECRET_ACCESS_KEY"}, :instance_role],
  region: "sa-east-1"

  config :ex_aws, :httpoison_opts,
    recv_timeout: 60_000,
    hackney: [recv_timeout: 60_000, pool: false]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
