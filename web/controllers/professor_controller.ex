defmodule Api.ProfessorController do
  use Api.Web, :controller
  require Logger

  plug :scrub_params, "professor" when action in [:create, :update]

  alias Api.{ Professor, Repo, User }

  def index(conn, _params) do
    professors = Repo.all(Professor)
    render(conn, "index_names.json", professors: professors)
  end

  def create(conn, %{"professor" => professor_params}) do
    changeset = Professor.changeset(%Professor{}, professor_params)

    Repo.transaction fn ->
      case Repo.insert(changeset) do
        {:ok, professor} ->

          first_name = professor.name
          |> String.split
          |> Enum.at(0)

          role = Repo.get_by!(Api.Role, name: "Professor")
          user = Ecto.build_assoc(professor, :user, %{username: first_name <> "-" <> User.generate_username_suffix_by_cpf(professor.cpf),
                                                      encrypted_password: Comeonin.Bcrypt.hashpwsalt(professor.cpf),
                                                      email: professor.email,
                                                      professor: professor,
                                                      role: role})
          Repo.insert!(user)

          conn
          |> put_status(:created)
          |> put_resp_header("location", professor_path(conn, :show, professor))
          |> render("show.json", professor: professor)
        {:error, changeset} ->
          conn
          |> put_status(:unprocessable_entity)
          |> render(Api.ChangesetView, "error.json", changeset: changeset)
      end
    end

    send_resp(conn, :no_content, "")

  end

  def show(conn, %{"id" => id}) do
    professor = Repo.get!(Professor, id)
    |> Repo.preload([ :subjects ])
    render(conn, "show.json", professor: professor)
  end

  def update(conn, %{"id" => id, "professor" => professor_params}) do
    professor = Repo.get!(Professor, id)
    |> Repo.preload(:classes)
    changeset = Professor.changeset(professor, professor_params)

    case Repo.update(changeset) do
      {:ok, professor} ->
        render(conn, "show.json", professor: professor)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    professor = Repo.get!(Professor, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(professor)

    send_resp(conn, :no_content, "")
  end

  def search(conn, %{"term" => term}) do
    professors = if String.length(term) > 0 do
      Professor.search(term)
    else
      Professor.search("")
    end

    render(conn, "search.json", professors: professors)
  end

  def search(conn, _term) do
    professors = Professor.search("")
    render(conn, "search.json", professors: professors)
  end

  def get_classes(conn, %{"id" => id}) do
    professor = Repo.get!(Professor, id)
    |> Repo.preload(:classes)
    classes = professor.classes
    |> Enum.map(fn c -> Api.Class.get_class_and_classplans_total(c.id, c.name) end)
    render(conn, Api.ClassView, "index_with_classplans_total.json", %{ classes: classes })
  end

  def get_subjects(conn, %{"id" => id, "class_id" => class_id}) do
    subjects = Professor.get_subjects_by_prof_class_id(id, class_id)
    render(conn, Api.SubjectView, "index_with_classplans_total_and_class_subject.json", %{ subjects: subjects })
  end

end
