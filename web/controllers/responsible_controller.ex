defmodule Api.ResponsibleController do
  use Api.Web, :controller

  alias Api.Responsible

  def index(conn, _params) do
    responsibles = Repo.all(Responsible)
    render(conn, "index.json", responsibles: responsibles)
  end

  def create(conn, %{"responsible" => responsible_params}) do
    changeset = Responsible.changeset(%Responsible{}, responsible_params)

    case Repo.insert(changeset) do
      {:ok, responsible} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", responsible_path(conn, :show, responsible))
        |> render("show.json", responsible: responsible)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    responsible = Repo.get!(Responsible, id)
    render(conn, "show.json", responsible: responsible)
  end

  def update(conn, %{"id" => id, "responsible" => responsible_params}) do
    responsible = Repo.get!(Responsible, id)
    changeset = Responsible.changeset(responsible, responsible_params)

    case Repo.update(changeset) do
      {:ok, responsible} ->
        render(conn, "show.json", responsible: responsible)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    responsible = Repo.get!(Responsible, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(responsible)

    send_resp(conn, :no_content, "")
  end
end
