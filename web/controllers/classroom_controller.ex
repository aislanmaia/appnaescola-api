defmodule Api.ClassroomController do
  use Api.Web, :controller

  plug :scrub_params, "classroom" when action in [:create, :update]
  import Kernel

  alias Api.Classroom

  def index(conn, params) do
    # classrooms = Repo.all(Classroom)
    classrooms = Classroom
      |> order_by([c], desc: c.name)
      |> Api.Repo.paginate(params)
    conn
    # |> Scrivener.Headers.paginate(classrooms, page_size: 20)
    |> render("index.json", classrooms: classrooms.entries, page_number: classrooms.page_number, total_pages: classrooms.total_pages)
  end

  def create(conn, %{"classroom" => classroom_params}) do
    changeset = Classroom.create_changeset(%Classroom{}, classroom_params)

    case Repo.insert(changeset) do
      {:ok, classroom} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", classroom_path(conn, :show, classroom))
        |> render("show_create.json", %{ classroom: classroom })
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    classroom = Repo.get!(Classroom, id)
    render(conn, "show.json", classroom: classroom)
  end

  def update(conn, %{"id" => id, "classroom" => classroom_params}) do
    classroom = Repo.get!(Classroom, id)
    |> Repo.preload(:classes)
    changeset = Classroom.changeset(classroom, classroom_params)

    case Repo.update(changeset) do
      {:ok, classroom} ->
        render(conn, "show.json", classroom: classroom)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    classroom = Repo.get!(Classroom, id)
		changeset = Classroom.delete_changeset(classroom)

		# Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    case Repo.delete(changeset) do
			{:ok, classroom} ->
				send_resp(conn, :no_content, "")
			{:error, changeset} ->
				conn
				|> put_status(:unprocessable_entity)
				|> render(Api.ChangesetView, "error.json", changeset: changeset)
				|> halt
		end

    # send_resp(conn, :no_content, "")
  end

	def batch_delete(conn, %{"classrooms" => classrooms}, _, _) when classrooms == [] do
    send_resp(conn, :no_content, "")
	end

	def batch_delete(conn, %{"classrooms" => classrooms}, error, changeset) when error == true do
		conn
		|> put_status(:unprocessable_entity)
		|> render(Api.ChangesetView, "error.json", changeset: changeset)
		|> halt
	end

  def batch_delete(conn, %{"classrooms" => classrooms}, error \\ false, changeset \\ %{}) when error == false do

		[c | rest] = classrooms
		id = Integer.to_string c["id"]
		classroom = Repo.get!(Classroom, id)
		changeset = Classroom.delete_changeset(classroom, c)
		case Repo.delete(changeset) do
			{:ok, classroom} ->
        batch_delete(conn, %{"classrooms" => rest}, false, classroom)
			{:error, changeset} ->
        batch_delete(conn, %{"classrooms" => rest}, true, changeset)
				# conn
				# |> put_status(:unprocessable_entity)
				# |> render(Api.ChangesetView, "error.json", changeset: changeset)
				# |> halt
		end

		# case Repo.update(changeset) do
    #   {:ok, classroom} ->
    #     render(conn, "show.json", classroom: classroom)
    #   {:error, changeset} ->
    #     conn
    #     |> put_status(:unprocessable_entity)
    #     |> render(Api.ChangesetView, "error.json", changeset: changeset)
    # end


    # send_resp(conn, :no_content, "")
		# conn
    # |> put_status(:unprocessable_entity)
    # |> render(Api.ChangesetView, "error.json", changeset: changeset)

  end

  def search(conn, %{"term" => term}) do
    classrooms = if String.length(term) > 0 do
      Classroom.search(term)
    else
      Classroom.search("")
    end

    render(conn, "search.json", classrooms: classrooms)
  end

  def search(conn, _term) do
    classrooms = Classroom.search("")
    render(conn, "search.json", classrooms: classrooms)
  end
end
