defmodule Api.ClassSubjectController do
  use Api.Web, :controller

  alias Api.ClassSubject

  def index(conn, %{"class_id" => class_id}) do
    class_subjects = Repo.all(from cs in ClassSubject, where: cs.class_id == ^class_id)
    render(conn, "index.json", class_subjects: class_subjects)
  end

  def create(conn, %{"class_subject" => class_subject_params}) do
    changeset = ClassSubject.changeset(%ClassSubject{}, class_subject_params)

    case Repo.insert(changeset) do
      {:ok, class_subject} ->
        conn
        |> put_status(:created)
        # |> put_resp_header("location", class_subject_path(conn, :show, class_subject))
        |> render("show.json", class_subject: class_subject)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def updating_by_class(conn, %{"class_subject" => class_subjects_params, "id" => class_id}) do
    Enum.each class_subjects_params, fn cs ->
      if not Map.has_key?(cs, "id") do
        IO.inspect "inserting"
        class_subject = %{"class_id" => class_id, "subject_id" => cs["subject_id"], "professor_id" => cs["professor_id"]}
        changeset = ClassSubject.changeset(%ClassSubject{}, class_subject)

        case Repo.insert(changeset) do
          {:ok, class_subject} ->
            render(conn, "show.json", class_subject: class_subject)
        end
      else
        if Map.has_key?(cs, "remove") do
          delete(conn, %{"id" => cs["id"] })
          render(conn, "class_subject_id.json", class_subject: %{id: cs["id"]})
        else
          IO.inspect "updating"
          class_subject = Repo.get!(ClassSubject, cs["id"])
          class_subject_struct = %{"class_id" => class_id, "subject_id" => cs["subject_id"], "professor_id" => cs["professor_id"]}
          changeset = ClassSubject.changeset(class_subject, class_subject_struct)

          case Repo.update(changeset) do
            {:ok, class_subject} ->
              render(conn, "show.json", class_subject: class_subject)
          end
        end
      end
    end
  end

  def show(conn, %{"id" => id}) do
    class_subject = Repo.get!(ClassSubject, id)
    render(conn, "show.json", class_subject: class_subject)
  end

  def update(conn, %{"id" => id, "class_subject" => class_subject_params}) do
    class_subject = Repo.get!(ClassSubject, id)
    changeset = ClassSubject.changeset(class_subject, class_subject_params)

    case Repo.update(changeset) do
      {:ok, class_subject} ->
        render(conn, "show.json", class_subject: class_subject)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    class_subject = Repo.get!(ClassSubject, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(class_subject)

    render(conn, "class_subject_id.json", class_subject: class_subject)
  end
end
