defmodule Api.ResponsabilityController do
  use Api.Web, :controller
  alias Api.{Student, Responsible, Responsability, Repo}

  def handle_responsibles(student, %{"resp1" => resp1, "resp2" => resp2}) do
    # %{"resp1" => resp1} = params
    # %{"resp2" => resp2} = params
    r1 = filter_responsible(resp1)
    r2 = filter_responsible(resp2)
    if length(r1) > 0 do
      resp1
      |> get_or_create_resp
      |> associate_with_resource(student)
    end

    if length(r2) > 0 do
      resp2
      |> get_or_create_resp
      |> associate_with_resource(student)
    end

  end

  defp filter_responsible(resp) do
    r = Enum.filter( resp, fn({key, value}) -> value != ""  end)
  end

  def get_or_create_resp(resp) do
    resp |> create_or_find_by_id
  end

  def create_or_find_by_id(resp) do
    changeset = Responsible.changeset %Responsible{}, resp

    try do
      Repo.get_by!(Responsible, id: resp.id)
    rescue
      KeyError -> add_responsible(changeset)
      Ecto.NoResultsError ->
        add_responsible(changeset)
    end

  end

  defp add_responsible(changeset) do
    case Repo.insert(changeset) do
      {:ok, responsible} -> responsible
    end
  end

  defp associate_with_resource(responsible, student) do
    # student
    # |> Repo.preload(:responsibles) # Load existing data
    # |> Ecto.Changeset.change() # Build the changeset
    # |> Ecto.Changeset.put_assoc(:responsibles, [responsible]) # Set the association
    # |> Repo.update!
    changeset = Responsability.changeset(%Responsability{}, %{student_id: student.id, responsible_id: responsible.id})
    case Repo.insert(changeset) do
      {:ok, assoc} -> assoc # Assoc was created!
      # {:error, changeset} -> # Handle the error
    end

  end

end
