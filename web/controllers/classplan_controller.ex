defmodule Api.ClassplanController do
  use Api.Web, :controller

  require Logger
  import String
  alias Api.Classplan

  def index(conn, _params) do
    classplans = Repo.all(Classplan)
    render(conn, "index.json", classplans: classplans)
  end

  def index(conn, %{"professor_id" => professor_id, "class_id" => class_id, "subject_id" => subject_id}) do
    IO.inspect "chamando index com params:"
    IO.inspect "professor_id: #{professor_id}"
    IO.inspect "class_id: #{class_id}"
    IO.inspect "subject_id: #{subject_id}"
    send_resp(conn, :no_content, "")
  end

  def create(conn, %{"classplan" => classplan_params}) do
    params = Classplan.parse_content_topics(classplan_params)
    changeset = Classplan.changeset(%Classplan{}, params)

    case Repo.insert(changeset) do
      {:ok, classplan} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", classplan_path(conn, :show, classplan))
        |> render("show.json", classplan: classplan)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    # classplan = Repo.get!(Classplan, id)
    # |> Repo.preload(:class_subject)
    classplan = Classplan.get_classplan_with_class_and_subject(id)

    render(conn, "show_classplan_class_subject.json", %{classplan: classplan})
  end

  def update(conn, %{"id" => id, "classplan" => classplan_params}) do
    cs = Repo.get!(Classplan, id)
    classplan_params = classplan_params
      |> Classplan.parse_attachment
      |> Classplan.parse_content_topics

    changeset = Classplan.update_changeset(cs, classplan_params)

    #Classplan.delete_attachment(classplan_params["attachment"], cs)
    # if classplan_params["attachment"] do
    #   # Deletes the preview file uploaded so then we wont have
    #   # orphan files in S3 storage.
    #   path = Api.Attachment.url({cs.attachment, cs})
    #   [path | _] = String.split path, "?" # strips the "?v=1234" from the URL string
    #   # For somehow, using the variable path doesn't working when passing to Api.Attachment.delete function below,
    #   # so that we're assigning it to another variable (path2).
    #   path2 = path
    #   Api.Attachment.delete({path2, cs})
    # end

    Classplan.delete_attachment(classplan_params["attachment"], cs)

    case Repo.update(changeset) do
      {:ok, classplan} ->
        # Sometimes, seems that the delete action for attachment gets interrupted (?)
        # when in classplan update process. Because that, we're dispatching again the action
        # to ensure the old attachment gets deleted on S3 storage, but just only when
        # a new attachment is sent in params.
        # if classplan_params["attachment"], do: Api.Attachment.delete({path2, cs})
        #Classplan.delete_attachment(classplan_params["attachment"], cs)

        render(conn, "show.json", classplan: classplan)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    classplan = Repo.get!(Classplan, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    classplan = Repo.delete!(classplan)

    Classplan.delete_attachment(classplan.attachment, classplan)

    send_resp(conn, :no_content, "")
  end

  def get_months_total_by_year(conn, %{"class_subject_id" => class_subject_id}) do
    data = Classplan.get_months_and_years(class_subject_id)
    render(conn, "months_total_by_year.json", %{ data: data })
  end

  def get_classplans_by(conn, %{"class_subject_id" => class_subject_id}) do
    classplans = Classplan.get_classplans_by(class_subject_id)
    render(conn, "index.json", classplans: classplans)
  end

  def get_classplans_by_class_subject_and_date(conn, %{"class_subject_id" => class_subject_id, "date" => date}) do
    classplans = Classplan.get_classplans_by(class_subject_id, date)
    render(conn, "index.json", classplans: classplans)
  end

end
