defmodule Api.StudentController do
  use Api.Web, :controller

  alias Api.{ Student, ResponsabilityController }

  def index(conn, _params) do
    students = Repo.all(Student)
    |> Repo.preload(:responsibles)
    render(conn, "index.json", students: students)
  end

  def create(conn, %{"student" => student_params}) do
    changeset = Student.changeset(%Student{}, student_params)

    case Repo.insert(changeset) do
      {:ok, student} ->
        ResponsabilityController.handle_responsibles(student, student_params)
        conn
        |> put_status(:created)
        |> put_resp_header("location", student_path(conn, :show, student))
        |> render("show.json", student: student)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    student = Repo.get!(Student, id)
    |> Repo.preload([ :responsibles, :class ])
    render(conn, "show_with_responsibles.json", student: student)
  end

  def update(conn, %{"id" => id, "student" => student_params}) do
    student = Repo.get!(Student, id)
    |> Repo.preload(:responsibles)
    changeset = Student.changeset(student, student_params)

    case Repo.update(changeset) do
      {:ok, student} ->
        render(conn, "show.json", student: student)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    student = Repo.get!(Student, id)
    |> Repo.preload(:responsibles)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(student)

    # send_resp(conn, :no_content, "")
    render(conn, "show_with_responsibles.json", student: student)
  end

  def search(conn, %{"term" => term, "options" => options}) do
    students = if String.length(term) > 0 do
      Student.search(term, options)
    else
      Student.search("", %{})
    end

    render(conn, "search.json", students: students)
  end

  def search(conn, _term) do
    students = Student.search("")
    render(conn, "search.json", students: students)
  end

  # def add_responsible(conn, %{"id" => id, "responsibility" => responsibility_params}) do
  #   IO.puts "id de aluno = #{id}"
  #   IO.inspect "Chamou add_responsibility com #{responsibility_params}"
  #   send_resp(conn, :no_content, "")
  # end

end
