defmodule Api.ClassController do
  use Api.Web, :controller

  alias Api.{Class, ProfessorClass}

  def index(conn, _params) do
    classes = Repo.all(Class)
    |> Repo.preload(:students)
    render(conn, "index.json", classes: classes)
  end

  def create(conn, %{"class" => class_params}) do
    class = Map.take(class_params, ["name", "classroom_id"])
    studentIds = class_params["studentIds"]
    professorIds = class_params["professorIds"]

    changeset = Class.changeset(%Class{}, class)

    case Repo.insert(changeset) do
      {:ok, class} ->

        class
        |> associate_with_students(studentIds)

        class
        |> associate_with_professors(professorIds)

        conn
        |> put_status(:created)
        |> put_resp_header("location", class_path(conn, :show, class))
        |> render("show.json", class: class)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
    # send_resp(conn, :no_content, "")
  end

  def show(conn, %{"id" => id}) do
    class = Repo.get!(Class, id)
    |> Repo.preload([ :students, :professors, :classroom ])
    render(conn, "show_with_associations.json", class: class)
  end

  def update(conn, %{"id" => id, "class" => class_params}) do
    class = Repo.get!(Class, id)
    # |> Repo.preload(:professors)
    |> Repo.preload([:students, :professors])

    studentIds = class_params["studentIds"]
    professorIds = class_params["professorIds"]
    changeset = Class.changeset(class, class_params)

    case Repo.update(changeset) do
      {:ok, class} ->

        class
        |> associate_with_students(studentIds)

        class
        |> associate_with_professors(professorIds)

        render(conn, "show.json", class: class)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Api.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    class = Repo.get!(Class, id)
    |> Repo.preload([ :students, :professors ])

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(class)

    render(conn, "show_with_associations.json", class: class)
  end

  def search(conn, %{"term" => term}) do
    classes = if String.length(term) > 0 do
      Class.search(term)
    else
      Class.search("")
    end

    render(conn, "search.json", classes: classes)
  end

  def search(conn, _term) do
    classes = Class.search("")
    render(conn, "search.json", classes: classes)
  end


  defp associate_with_students(class, studentIds) do
    # |> Enum.find_value(fn({k, v}) -> if k == "added" do IO.inspect v end end)
    # |> Enum.into(%{})
    added = studentIds["added"]
    removed = studentIds["removed"]
    if Enum.any?(removed) do
      from(s in Api.Student, where: s.id in ^removed, update: [set: [class_id: nil]])
      |> Repo.update_all([])
    end
    if Enum.any?(added) do
      from(s in Api.Student, where: s.id in ^added, update: [set: [class_id: ^class.id]])
      |> Repo.update_all([])
    end
  end

  defp associate_with_professors(class, professorIds) do
    added = professorIds["added"]
    removed = professorIds["removed"]
    if Enum.any?(added) do
      profsToAdd = Repo.all(from(p in Api.Professor, where: p.id in ^added, preload: :classes))

      Enum.each(profsToAdd, fn professor ->
        IO.inspect professor
        # class
        # |> Repo.preload(:professors)
        # |> Ecto.Changeset.change() # Build the changeset
        # |> Ecto.Changeset.put_assoc(:professors, [professor]) # Set the association
        # |> Repo.update!

        changeset = ProfessorClass.changeset(%ProfessorClass{}, %{professor_id: professor.id, class_id: class.id})
        case Repo.insert(changeset) do
          {:ok, assoc} -> assoc # Assoc was created!
          # {:error, changeset} -> # Handle the error
        end
      end)
    end

    if Enum.any?(removed) do
      profClassToRemove = Repo.all(from(pc in ProfessorClass, where: pc.professor_id in ^removed and pc.class_id == ^class.id))

      Enum.each(profClassToRemove, fn prof_class ->
        # changeset = ProfessorClass.changeset(%ProfessorClass{}, %{professor_id: professor.id, class_id: class.id})
        case Repo.delete(prof_class) do
          {:ok, assoc} -> assoc # Assoc was created!
          # {:error, changeset} -> # Handle the error
        end
      end)
    end

  end

  def count_class(conn, _params) do
    count = Class.get_class_count()
    render(conn, "count.json", count: count)
  end

  def get_professors(conn, %{"id" => id}) do
    class = Repo.get!(Class, id)
    |> Repo.preload([ :professors ])
    # professors = class.professors
    render(conn, "class_professors.json", class: class)
  end

  def get_subjects(conn, %{"id" => id}) do
    class_subjects = Repo.all(from cs in Api.ClassSubject, where: cs.class_id == ^id, preload: [:professor, :subject])
    render(conn, "show_subject_and_professor.json", class_subjects: class_subjects)
  end

end
