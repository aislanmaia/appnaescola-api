defmodule Api.File do
  use Arc.Definition

  # Include ecto support (requires package arc_ecto installed):
  use Arc.Ecto.Definition
  import String

  @versions [:original]
  @acl :public_read

  # To add a thumbnail version:
  # @versions [:original, :thumb]

  # Whitelist file extensions:
  # def validate({file, _}) do
  #   ~w(.jpg .jpeg .gif .png) |> Enum.member?(Path.extname(file.file_name))
  # end

  # Define a thumbnail transformation:
  # def transform(:thumb, _) do
  #   {:convert, "-strip -thumbnail 250x250^ -gravity center -extent 250x250 -format png", :png}
  # end

  # Override the persisted filenames:
  def filename(version, { file, scope }) do
    file_name = Path.basename(file.file_name, Path.extname(file.file_name))
    # "#{file_name |> String.normalize(:nfd) |> String.replace(~r/[^A-z|1-9|ºª()|.\s]/u, "")}"

    # just convert the filename to unicode to be accepted by S3
    "#{file_name |> String.normalize(:nfd) |> String.replace(~r//u, "")}"
  end

  # Override the storage directory:
  def storage_dir(version, {file, scope}) do
    info = IEx.Info.info(scope)
    type = Enum.find_value(info, fn({k, v}) -> v end)
    if type == "Api.Classplan" do 
      IO.inspect(IEx.Info.info(scope))
      "uploads/classplans/#{scope.uuid}"
    else
      IO.inspect(IEx.Info.info(scope))
      "uploads/activities/#{scope.uuid}"
    end

  end

  # Provide a default URL if there hasn't been a file uploaded
  # def default_url(version, scope) do
  #   "/images/avatars/default_#{version}.png"
  # end

  # Specify custom headers for s3 objects
  # Available options are [:cache_control, :content_disposition,
  #    :content_encoding, :content_length, :content_type,
  #    :expect, :expires, :storage_class, :website_redirect_location]
  #
  # def s3_object_headers(version, {file, scope}) do
  #   [content_type: Plug.MIME.path(file.file_name)]
  # end
end
