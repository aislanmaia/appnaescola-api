defmodule Api.ClassSubject do
  use Api.Web, :model

  schema "class_subjects" do
    belongs_to :class, Api.Class, on_replace: :delete
    belongs_to :subject, Api.Subject, on_replace: :delete
    belongs_to :professor, Api.Professor

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:class_id, :subject_id, :professor_id])
    |> validate_required([:class_id, :subject_id])
    |> foreign_key_constraint(:class_id, message: "Turma não existe na base de dados!", name: "class_subjects_class_id_fkey")
    |> foreign_key_constraint(:subject_id, message: "Matéria não existe na base de dados!", name: "class_subjects_subject_id_fkey")
    |> foreign_key_constraint(:professor_id, message: "Professor não existe na base de dados!", name: "class_subjects_professor_id_fkey")
  end
end
