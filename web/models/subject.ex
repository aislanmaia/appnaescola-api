defmodule Api.Subject do
  use Api.Web, :model

  schema "subjects" do
    field :name, :string
    field :optional, :boolean, default: false
		field :uuid, :string, virtual: true

    many_to_many :classes, Api.Class, join_through: Api.ClassSubject
    many_to_many :professors, Api.Professor, join_through: Api.ClassSubject

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :optional, :uuid])
    |> validate_required([:name, :optional])
    |> unique_constraint(:name, message: "Este nome já foi definido anteriormente.")
  end

  def search(term, limit \\ 10) do
    query = from(c in __MODULE__, select: struct(c, [:id, :name]), where: ilike(c.name, ^"%#{term}%"), order_by: [asc: c.name ], limit: ^limit)
    Api.Repo.all(query)
  end

end
