defmodule Api.Classplan do
  use Api.Web, :model
  use Arc.Ecto.Schema

  schema "classplans" do
    # field :content_topics, :map
    embeds_many :content_topics, Api.ContentTopics, on_replace: :delete
    field :begin_date, Ecto.Date
    field :end_date, Ecto.Date
    field :attachment, Api.Attachment.Type
    field :uuid, :string
    belongs_to :class_subject, Api.ClassSubject

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:begin_date, :end_date, :class_subject_id])
    |> cast_embed(:content_topics)
    |> put_change(:uuid, UUID.uuid4())
    |> cast_attachments(params, [:attachment])
    |> validate_required([:begin_date, :end_date, :attachment])
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def update_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:begin_date, :end_date, :class_subject_id])
    |> cast_embed(:content_topics)
    |> cast_attachments(params, [:attachment])
    |> validate_required([:begin_date, :end_date, :attachment])
  end



  # def generate_uuid(changeset, key) do
  #   IO.inspect "changeset in generate_uuid"
  #   IO.inspect changeset
  #   if not changeset.action == "update" do
  #     put_change(changeset, key, UUID.uuid4())
  #   end
  # end

  def parse_content_topics(params) do
    Enum.reduce params, %{}, fn {k, v}, acc ->
      if k == "content_topics" and is_bitstring(v) do
        {_, topicsList } = Poison.decode v
        topics = Enum.map topicsList, fn (v) ->
          if is_bitstring(v) do
            %{text: v}
          else
            v
          end
        end
        Map.put(acc, k, topics)
      else
        Map.put(acc, k, v)
      end
    end
  end

  # PARECE QUE SE ENVIAR UM UPDATE COM ATTACHMENT NÃO MODIFICADO,
  # ELE VIRÁ NO FORMATO "[object OBJECT]" (STRING) E O SERVER S3 EXCLUI O
  # ARQUIVO QUE JÁ ESTAVA LÁ NA STORAGE.
  # ENTÃO, TALVEZ SEJA NECESSÁRIO CHECAR O TIPO DO PARAMETRO ENVIADO
  # PELO FRONTEND E ENTÃO RETIRÁ-LO DOS PARAMETROS CASO NECESSÁRIO.
  def parse_attachment(params) do
    if is_bitstring(params["attachment"]), do: params = Map.delete(params, "attachment"), else: params
  end

  def get_months_and_years(class_subject_id) do
    if is_bitstring(class_subject_id), do: class_subject_id = String.to_integer class_subject_id

    # Execute query and returns it in the example form:
    # %Postgrex.Result{columns: ["mon", "year", "qty"], command: :select,
    #  connection_id: 29173, num_rows: 3,
    #  rows: [["Jun", 2016.0, 1], ["Jun", 2017.0, 3], ["Mar", 2016.0, 1]]}
    {:ok, results}  = Ecto.Adapters.SQL.query(Api.Repo,
      "SELECT to_char(begin_date, 'Mon') as mon, extract(year from begin_date) as year, count (*) as Qty
      from classplans
      where class_subject_id::integer = $1::integer
      group by mon, year",
      [ class_subject_id ]
    )
    # Enum.group_by(results.rows, fn [_, year, _] -> %{year: round(year)} end, fn [x, _, z] -> %{mon: x, qtd: z} end)
    Enum.group_by(results.rows, fn [_, year, _] -> round(year) |> Integer.to_string end, fn [x, _, z] -> %{mon: x, qtd: z} end)
  end

  def get_classplans_by(class_subject_id) do
    query = from(cs in __MODULE__, where: cs.class_subject_id == ^class_subject_id)
    Api.Repo.all(query)
  end

  def get_classplans_by(class_subject_id, date) do
    [year, month] = String.split(date, "-")
    begin_date = "#{year}-#{month}-01"
    day = Calendar.ISO.days_in_month(String.to_integer(year), String.to_integer(month))
    end_date = "#{year}-#{month}-#{day}"
    query = from(
      cs in __MODULE__,
      where: cs.class_subject_id == ^class_subject_id and
            cs.begin_date >= ^begin_date and cs.begin_date <= ^end_date
    )
    Api.Repo.all(query)
  end

  def get_classplan_with_class_and_subject(id) do
    classplan = Api.Repo.one from classplan in __MODULE__,
      where: classplan.id == ^id,
      left_join: class_subject in assoc(classplan, :class_subject),
      left_join: class in assoc(class_subject, :class),
      left_join: subject in assoc(class_subject, :subject),
      preload: [class_subject: {class_subject, class: class, subject: subject}]
  end

  def delete_attachment(attachment, classplan) do
    if attachment do
      # Deletes the preview file uploaded so then we wont have
      # orphan files in S3 storage.
      list = ExAws.S3.list_objects(System.get_env("AWS_S3_BUCKET_UPLOADS"), prefix: "uploads/classplans")
      |> ExAws.stream!
      |> Enum.to_list #=> [list, of, objects]

      Enum.each list, fn obj ->
        [_, _, _, file_name] = String.split obj.key, "/"
        if file_name == classplan.attachment.file_name do
          ExAws.S3.delete_object(System.get_env("AWS_S3_BUCKET_UPLOADS"), obj.key)
          |> ExAws.request()
        end
      end
    end
  end


end

defmodule Api.ContentTopics do
  use Api.Web, :model

  embedded_schema do
    field :text, :string
    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:text])
  end

end

