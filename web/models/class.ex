defmodule Api.Class do
  use Api.Web, :model

  schema "classes" do
    field :name, :string
    belongs_to :classroom, Api.Classroom

    has_many :students, Api.Student
    many_to_many :professors, Api.Professor, join_through: Api.ProfessorClass, on_delete: :delete_all
    many_to_many :subjects, Api.Subject, join_through: Api.ClassSubject, on_delete: :delete_all
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :classroom_id])
    # |> Ecto.Changeset.cast_assoc(:students, required: false)
    # |> Ecto.Changeset.put_assoc(:professors, parse_professor_ids(params), required: false)
    |> validate_required([:name])
		|> assoc_constraint(:classroom_id, message: "Esta sala está associada.", name: "classes_classroom_id_fkey")
		|> foreign_key_constraint(:classroom, name: "classes_classroom_id_fkey", message: "Esta sala está referenciada ou em uso.")
  end

  # def parse_professor_ids(params) do
  #   # (params["professorIds"] || [])
  #   professors = params["professorIds"]
  #   added = professors["added"]
  #   |> Enum.map(&get_professor/1)
  # end

  # defp get_professor(id) do
  #   Api.Repo.get_by(Api.Professor, id: id)
  # end

  def search(term, limit \\ 10) do
    query = from(c in __MODULE__, select: struct(c, [:id, :name]), where: ilike(c.name, ^"%#{term}%"), order_by: [asc: c.name ], limit: ^limit)
    Api.Repo.all(query)
  end

  def get_class_count() do
    Api.Repo.one(from c in __MODULE__, select: count("*"))
  end

  def get_class_and_classplans_total(class_id, class_name) do
    # Esta função retornará classes e planos de aula atrelados um ao outro,
    # se houver ao menos um plano de aula associado.
    # Se não há nenhum plano de aula, retorna o id e o nome da classe
    # com o total 0 para planos de aula.
    {:ok, result} = Ecto.Adapters.SQL.query(
      Api.Repo,
      "select c.id, c.name,
      (select count(cp.id) from classplans as cp where cp.class_subject_id in
      (select cs.id from class_subjects as cs where cs.class_id in (select c.id from classes as c where c.id::integer = $1::integer))) as classplans_total
      from classes as c, class_subjects as cs, classplans as cp
      where c.id::integer = $1::integer
      group by c.id",
      [ class_id ]
    )
    
    if Enum.any?(result.rows) do
      [id, name, total] = hd result.rows 
      %{id: id, name: name, total: total}
    else 
      %{id: class_id, name: class_name, total: 0}
    end
  end

end
