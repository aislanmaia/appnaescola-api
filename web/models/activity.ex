defmodule Api.Activity do
  use Api.Web, :model
  use Arc.Ecto.Schema

  schema "activities" do
    field :name, :string
    field :description, :string
    field :due_date, Ecto.Date
    field :file, Api.File.Type
    field :uuid, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :description, :due_date])
    |> put_change(:uuid, UUID.uuid4())
    |> cast_attachments(params, [:file])
    |> validate_required([:name, :description, :due_date])
  end

  def update_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :description, :due_date])
    |> cast_attachments(params, [:file])
    |> validate_required([:name, :description, :due_date])
  end

  def delete_file(file, activity) do
    if file do
      # Deletes the preview file uploaded so then we wont have
      # orphan files in S3 storage.
      list = ExAws.S3.list_objects(System.get_env("AWS_S3_BUCKET_UPLOADS"), prefix: "uploads/activities")
      |> ExAws.stream!
      |> Enum.to_list #=> [list, of, objects]

      Enum.each list, fn obj ->
        [_, _, _, file_name] = String.split obj.key, "/"
        if file_name == activity.file.file_name do
          ExAws.S3.delete_object(System.get_env("AWS_S3_BUCKET_UPLOADS"), obj.key)
          |> ExAws.request()
        end
      end
    end
  end

  # PARECE QUE SE ENVIAR UM UPDATE COM FILE NÃO MODIFICADO,
  # ELE VIRÁ NO FORMATO "[object OBJECT]" (STRING) E O SERVER S3 EXCLUI O
  # ARQUIVO QUE JÁ ESTAVA LÁ NA STORAGE.
  # ENTÃO, TALVEZ SEJA NECESSÁRIO CHECAR O TIPO DO PARAMETRO ENVIADO
  # PELO FRONTEND E ENTÃO RETIRÁ-LO DOS PARAMETROS CASO NECESSÁRIO.
  def parse_file(params) do
    if is_bitstring(params["file"]), do: params = Map.delete(params, "file"), else: params
  end


end
