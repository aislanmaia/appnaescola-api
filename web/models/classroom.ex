defmodule Api.Classroom do
  use Api.Web, :model

  @derive {Poison.Encoder, only: [:name, :classes]}
  schema "classrooms" do
    field :name, :string
		field :uuid, :string, virtual: true

    has_many :classes, Api.Class

    timestamps()
  end

  @required_fields ~w(name)
  @optional_fields ~w()

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields)
    |> Ecto.Changeset.cast_assoc(:classes, required: false)
    |> validate_required([:name])
    |> unique_constraint(:name, message: "Este nome já foi definido anteriormente.")
		|> foreign_key_constraint(:classes, message: "Esta sala está referenciada ou em uso.", name: :classes_classroom_id_fkey)
  end


  def create_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :uuid])
    |> Ecto.Changeset.cast_assoc(:classes, required: false)
    |> validate_required([:name])
    |> unique_constraint(:name, message: "Este nome já foi definido anteriormente.")
  end

	def delete_changeset(struct, params \\ %{}) do
		struct
		|> cast(params, @required_fields)
		|> check_foreign_key_constraint
	end

  def search(term, limit \\ 10) do
    query = from(c in __MODULE__, select: struct(c, [:id, :name]), where: ilike(c.name, ^"%#{term}%"), order_by: [asc: c.name ], limit: ^limit)
    Api.Repo.all(query)
  end

	defp check_foreign_key_constraint(changeset) do
		name = get_field(changeset, :name)
		message = "#{name} está referenciada ou em uso."

		changeset
			|> foreign_key_constraint(:classes, message: message, name: :classes_classroom_id_fkey)
	end

end
