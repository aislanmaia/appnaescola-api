defmodule Api.Professor do
  use Api.Web, :model

  alias Api.{User}

  @derive {Poison.Encoder, only: [:name, :birthdate, :phone, :email, :sex, :classes, :subjects, :user]}
  schema "professors" do
    field :name, :string
    field :birthdate, Ecto.Date
    field :phone, :string
    field :email, :string
    field :cpf, :string
    field :sex, :string

    many_to_many :classes, Api.Class, join_through: Api.ProfessorClass
    many_to_many :subjects, Api.Subject, join_through: Api.ClassSubject
    belongs_to :user, Api.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :birthdate, :phone, :email, :cpf, :sex])
    |> Ecto.Changeset.cast_assoc(:classes, required: false)
    |> Ecto.Changeset.cast_assoc(:subjects, required: false)
    |> Ecto.Changeset.cast_assoc(:user, required: false)
    |> validate_required([:name, :birthdate, :phone, :email, :cpf, :sex])
    |> unique_constraint(:cpf)
  end

  # def registration_changeset_with_user(struct, params \\ %{}) do
  #   struct
  #   |> cast(params, [:name, :birthdate, :phone, :email, :cpf, :sex])
  #   |> Ecto.Changeset.cast_assoc(:classes, required: false)
  #   |> validate_required([:name, :birthdate, :phone, :email, :cpf, :sex])
  #   |> unique_constraint(:cpf)
  #   |> 
  # end

  def search(term, limit \\ 10) do
    query = from(p in __MODULE__, preload: [:classes], select: struct(p, [:id, :name]), where: ilike(p.name, ^"%#{term}%"), order_by: [asc: p.name ], limit: ^limit)
    Api.Repo.all(query)
  end

  def get_subjects_by_prof_class_id(professor_id, class_id) do
    if is_bitstring(professor_id), do: professor_id = String.to_integer professor_id
    if is_bitstring(class_id), do: class_id = String.to_integer class_id
    {:ok, result} = Ecto.Adapters.SQL.query(
      Api.Repo,
      "select s.id, s.name, (select count(cp.id) as classplans_total from classplans as cp
                             where cp.class_subject_id in
                             (select id from class_subjects
                              where professor_id::integer = $1::integer and class_id::integer = $2::integer and subject_id = s.id)),
                            (select id from class_subjects
                             where professor_id::integer = $1::integer and class_id::integer = $2::integer and subject_id = s.id)
      from subjects as s
      where s.id in (SELECT subject_id FROM class_subjects
                     where professor_id::integer = $1::integer and class_id::integer = $2::integer)",
      [professor_id, class_id]
    )
    Enum.map result.rows, fn row ->
      [id, name, classplans_total, class_subject_id] = row
      %{id: id, name: name, classplans_total: classplans_total, class_subject_id: class_subject_id}
    end
  end


end
