defmodule Api.Student do
  use Api.Web, :model

  alias Api.{Responsible, Repo}

  @derive {Poison.Encoder, only: [:name, :birthdate, :phone, :email, :year, :sex, :responsibles, :class]}
  schema "students" do
    field :name, :string
    field :birthdate, Ecto.Date
    field :phone, :string
    field :email, :string
    field :sex, :string

    many_to_many :responsibles, Api.Responsible, join_through: Api.Responsability, on_delete: :delete_all
    belongs_to :class, Api.Class

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :birthdate, :phone, :email, :sex, :class_id])
    |> Ecto.Changeset.cast_assoc(:responsibles, required: false)
    |> validate_required([:name])
    |> foreign_key_constraint(:class_id, message: "Turma desejada não existe na base de dados!")
  end

  def search(term, limit \\ 10, opts) do
    if opts["no_class"] do
      query = from(c in __MODULE__, select: struct(c, [:id, :name]), where: ilike(c.name, ^"%#{term}%") and is_nil(c.class_id), order_by: [asc: c.name ], limit: ^limit)
    else
      query = from(c in __MODULE__, select: struct(c, [:id, :name]), where: ilike(c.name, ^"%#{term}%"), order_by: [asc: c.name ], limit: ^limit)
    end
    Api.Repo.all(query)
  end

end
