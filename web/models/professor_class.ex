defmodule Api.ProfessorClass do
  use Api.Web, :model

  schema "professors_classes" do
    belongs_to :professor, Api.Professor, on_replace: :delete
    belongs_to :class, Api.Class, on_replace: :delete

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:professor_id, :class_id])
    |> validate_required([:professor_id, :class_id])
    |> foreign_key_constraint(:professor_id, message: "Professor não existe na base de dados!")
    |> foreign_key_constraint(:class_id, message: "Turma não existe na base de dados!")
  end
end
