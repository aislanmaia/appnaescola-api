defmodule Api.Responsible do
  use Api.Web, :model

  @derive {Poison.Encoder, only: [:name, :kinship, :phone, :cpf, :email, :students]}
  schema "responsibles" do
    field :name, :string
    field :kinship, :string
    field :phone, :string
    field :cpf, :string
    field :email, :string

    many_to_many :students, Api.Student, join_through: Api.Responsability, on_delete: :delete_all

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :kinship, :phone, :cpf, :email])
    # |> Ecto.Changeset.cast_assoc(:students, required: false)
    |> validate_required([:name, :kinship])
  end
end
