defmodule Api.Responsability do
  use Api.Web, :model

  schema "responsabilities" do
    belongs_to :responsible, Api.Responsible
    belongs_to :student, Api.Student

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:responsible_id, :student_id])
    |> validate_required([:responsible_id, :student_id])
  end
end
