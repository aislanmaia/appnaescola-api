defmodule Api.User do
  use Api.Web, :model

  require Logger

  schema "users" do
    field :email, :string
    field :encrypted_password, :string
    field :username, :string
    field :password, :string, virtual: true

    belongs_to :role, Api.Role
    has_one :professor, Api.Professor

    timestamps()
  end

  @required_fields ~w(email username role_id)
  @optional_fields ~w()


  def registration_changeset(model, params) do
    model
    |> changeset(params)
    |> cast(params, ~w(password), [])
    |> validate_length(:password, min: 6, max: 100)
    |> put_encrypted_pw
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @required_fields, @optional_fields)
    |> validate_required([:username, :email, :role_id])
    |> validate_format(:email, ~r/@/)
    |> validate_length(:username, min: 1, max: 20)
    |> update_change(:email, &String.downcase/1)
    |> unique_constraint(:email)
    |> update_change(:username, &String.downcase/1)
    |> unique_constraint(:username)
  end

  defp put_encrypted_pw(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :encrypted_password, Comeonin.Bcrypt.hashpwsalt(pass))
      _ ->
        changeset
    end
  end

  @doc """
  This function generates a suffix for usernames based on cpf and the system time.
  The calculation occurs by sum the cpf number and the system time number
  returning the result.
  """
  def generate_username_suffix_by_cpf(cpf) do
    {cpf_number, _} = Integer.parse(cpf)
    Logger.info "Debugging generate_username_suffix_by_cpf"
    Logger.debug "cpf_number = #{inspect(cpf_number)}"
    # {time, _} = Integer.parse(System.system_time)
    to_string( cpf_number + System.system_time )
  end
end
