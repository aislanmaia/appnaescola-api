defmodule Api.Router do
  use Api.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  # This pipeline takes care to verify our Authorization header and
  # load the resource using our serializer
  pipeline :api_auth do
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
  end

  scope "/", Api do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/api", Api do
    pipe_through :api

    resources "/activities", ActivityController, except: [:new, :edit]

    resources "/students", StudentController, except: [:new, :edit]
    post "/students/:id/responsibles/:responsibleId", StudentController, :add_responsible
    resources "/responsibles", ResponsibleController, except: [:new, :edit]
    resources "/classes", ClassController, except: [:new] #do

    # resources "/subjects", ClassSubjectController, except: [:new, :edit], as: :subject
    get "/classplans/months_by_year/", ClassplanController, :get_months_total_by_year
    get "/classplans/class_subject/:class_subject_id", ClassplanController, :get_classplans_by
    get "/classplans/class_subject/:class_subject_id/date/:date", ClassplanController, :get_classplans_by_class_subject_and_date
    get "/classes/:id/professors", ClassController, :get_professors
    get "/classes/:id/subjects", ClassController, :get_subjects
    get "/professors/:id/classes", ProfessorController, :get_classes
    get "/professors/:id/classes/:class_id/subjects", ProfessorController, :get_subjects
    post "/classes/:id/subjects/:subject_id", ClassController, :add_subject
    post "/classes/:id/subjects/", ClassSubjectController, :updating_by_class
    post "/classes/count", ClassController, :count_class
    # resources "/class_subjects", ClassSubjectController, except: [:new, :edit]
    resources "/professors", ProfessorController, except: [:new]
    resources "/subjects", SubjectController, except: [:new]
    resources "/classrooms", ClassroomController, except: [:new]
    resources "/users", UserController, except: [:show, :index, :new, :edit]
    resources "/classplans", ClassplanController, except: [:new, :edit]
    get "/users/:id/professor", UserController, :get_professor
    post "/classrooms/batch_delete", ClassroomController, :batch_delete

  end

  scope "/api/search", Api do
    pipe_through :api

    get "/classrooms/:term", ClassroomController, :search, as: :classroom_search
    get "/classrooms/", ClassroomController, :search, as: :classroom_search
    get "/professors/:term", ProfessorController, :search, as: :professor_search
    get "/professors", ProfessorController, :search, as: :professor_search
    post "/students/", StudentController, :search, as: :student_search
    post "/students", StudentController, :search, as: :student_search
    get "/classes/:term", ClassController, :search, as: :class_search
    get "/classes/", ClassController, :search, as: :class_search
    get "/subjects/:term", SubjectController, :search, as: :subject_search
    get "/subjects/", SubjectController, :search, as: :subject_search

  end

  scope "/auth", Api do
    pipe_through [:api, :api_auth]

    get "/me", AuthController, :me
    post "/:identity/callback", AuthController, :callback
    delete "/signout", AuthController, :delete
  end
  # Other scopes may use custom stacks.
  # scope "/api", Api do
  #   pipe_through :api
  # end
end
