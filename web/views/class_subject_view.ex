defmodule Api.ClassSubjectView do
  use Api.Web, :view

  def render("index.json", %{class_subjects: class_subjects}) do
    %{data: render_many(class_subjects, Api.ClassSubjectView, "class_subject.json")}
  end

  def render("show.json", %{class_subject: class_subject}) do
    %{data: render_one(class_subject, Api.ClassSubjectView, "class_subject.json")}
  end

  def render("class_subject_id.json", %{class_subject: class_subject}) do
    %{id: class_subject.id}
  end

  def render("class_subject.json", %{class_subject: class_subject}) do
    %{id: class_subject.id,
      class_id: class_subject.class_id,
      subject_id: class_subject.subject_id,
      professor_id: class_subject.professor_id}
  end

  def render("subjects_professors.json", %{class_subject: class_subject}) do
    if class_subject.professor do
      %{class_subject_id: class_subject.id,
        subject: %{
          id: class_subject.subject.id,
          name: class_subject.subject.name
        },
        professor: %{
          id: class_subject.professor.id,
          name: class_subject.professor.name
        }}
    else
      %{
        class_subject_id: class_subject.id,
        subject: %{
          id: class_subject.subject.id,
          name: class_subject.subject.name
        }
      }
    end

  end
end
