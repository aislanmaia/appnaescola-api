defmodule Api.ResponsibleView do
  use Api.Web, :view

  def render("index.json", %{responsibles: responsibles}) do
    %{data: render_many(responsibles, Api.ResponsibleView, "responsible.json")}
  end

  def render("show.json", %{responsible: responsible}) do
    %{data: render_one(responsible, Api.ResponsibleView, "responsible.json")}
  end

  def render("responsible.json", %{responsible: responsible}) do
    %{id: responsible.id,
      name: responsible.name,
      kinship: responsible.kinship,
      phone: responsible.phone,
      cpf: responsible.cpf,
      email: responsible.email}
  end
end
