defmodule Api.ProfessorView do
  use Api.Web, :view

  def render("index.json", %{professors: professors}) do
    %{data: render_many(professors, Api.ProfessorView, "professor.json")}
  end

  def render("show.json", %{professor: professor}) do
    %{data: render_one(professor, Api.ProfessorView, "professor.json")}
  end

  def render("index_names.json", %{professors: professors}) do
    %{data: render_many(professors, Api.ProfessorView, "professor_name.json")}
  end

  def render("search.json", %{professors: professors}) do
    %{data: render_many(professors, Api.ProfessorView, "professor.json")}
  end

  def render("professor.json", %{professor: professor}) do
    # Returns just values different other than nil
    professor
    |> Map.delete(:__meta__)
    |> Map.from_struct
    |> Enum.reject(fn {_, v} -> v == nil end)
    |> Enum.reject(fn {k, _} -> k == :classes end)
    |> Enum.reject(fn {k, _} -> k == :subjects end)
    |> Enum.reject(fn {k, _} -> k == :user end)
    |> Enum.into(%{})
    # %{id: professor.id if professor.id,
    #   name: professor.name if professor.name,
    #   birthdate: professor.birthdate if professor.birthdate,
    #   phone: professor.phone,
    #   email: professor.email,
    #   cpf: professor.cpf,
    # sex: professor.sex}
  end

  def render("professor_name.json", %{professor: professor}) do
    %{id: professor.id,
      name: professor.name}
  end

  def render("professor_classes_with_classplans_count.json", %{classes: classes}) do
  end

end
