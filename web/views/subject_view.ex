defmodule Api.SubjectView do
  use Api.Web, :view

  def render("index.json", %{subjects: subjects}) do
    %{data: render_many(subjects, Api.SubjectView, "subject.json")}
  end

  def render("index_id_name.json", %{subjects: subjects}) do
    %{data: render_many(subjects, Api.SubjectView, "subject_name.json")}
  end

  def render("index_with_classplans_total_and_class_subject.json", %{subjects: subjects}) do
    %{data: render_many(subjects, Api.SubjectView, "subject_classplans_total_and_class_subject.json")}
  end

  def render("show.json", %{subject: subject}) do
    %{data: render_one(subject, Api.SubjectView, "subject.json")}
  end

  def render("show_create.json", %{subject: subject}) do
    %{data: render_one(subject, Api.SubjectView, "subject_create.json")}
  end

  def render("subject.json", %{subject: subject}) do
    %{id: subject.id,
      name: subject.name,
      optional: subject.optional}
  end

  def render("subject_create.json", %{subject: subject}) do
    %{id: subject.id,
      name: subject.name,
      optional: subject.optional,
		  uuid: subject.uuid}
  end

  def render("subject_name.json", %{subject: subject}) do
    %{id: subject.id,
      name: subject.name}
  end

  def render("subject_classplans_total_and_class_subject.json", %{subject: subject}) do
    %{id: subject.id,
      name: subject.name,
      classplans_total: subject.classplans_total,
      class_subject_id: subject.class_subject_id}
  end

  def render("search.json", %{subjects: subjects}) do
    %{data: render_many(subjects, Api.SubjectView, "subject_name.json")}
  end

end
