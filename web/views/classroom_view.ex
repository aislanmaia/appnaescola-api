defmodule Api.ClassroomView do
  use Api.Web, :view

  def render("index.json", %{classrooms: classrooms, page_number: page_number, total_pages: total_pages}) do
    %{data: render_many(classrooms, Api.ClassroomView, "classroom.json"), page_number: page_number, total_pages: total_pages}
  end

  def render("show.json", %{classroom: classroom}) do
    %{data: render_one(classroom, Api.ClassroomView, "classroom.json")}
  end

  def render("show_create.json", %{classroom: classroom}) do
    %{data: render_one(classroom, Api.ClassroomView, "classroom_create.json")}
  end

  def render("search.json", %{classrooms: classrooms}) do
    %{data: render_many(classrooms, Api.ClassroomView, "classroom.json")}
  end

  def render("search_name.json", %{classrooms: classrooms}) do
    %{data: render_many(classrooms, Api.ClassroomView, "classroom_only_name.json")}
  end

  def render("classroom.json", %{classroom: classroom}) do
    %{id: classroom.id,
      name: classroom.name}
  end

  def render("classroom_create.json", %{classroom: classroom}) do
    %{id: classroom.id,
      name: classroom.name,
		  uuid: classroom.uuid}
  end

  def render("classroom_only_name.json", %{classroom: classroom}) do
    %{name: classroom.name}
  end
end
