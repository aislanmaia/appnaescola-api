defmodule Api.ClassplanView do
  use Api.Web, :view

  def render("index.json", %{classplans: classplans}) do
    %{data: render_many(classplans, Api.ClassplanView, "classplan.json")}
  end

  def render("show.json", %{classplan: classplan}) do
    %{data: render_one(classplan, Api.ClassplanView, "classplan.json")}
  end

  def render("show_classplan_class_subject.json", %{classplan: classplan}) do
    %{data: render_one(classplan, Api.ClassplanView, "classplan_class_subject.json")}
  end

  def render("classplan.json", %{classplan: classplan}) do
    %{id: classplan.id,
      content_topics: map_content_topics(classplan.content_topics),
      begin_date: classplan.begin_date,
      end_date: classplan.end_date,
      class_subject_id: classplan.class_subject_id,
      attachment: classplan.attachment,
      url: Api.Attachment.url({classplan.attachment, classplan})}
  end

  def render("classplan_class_subject.json", %{classplan: classplan}) do
    %{id: classplan.id,
      class: %{
        id: classplan.class_subject.class.id,
        name: classplan.class_subject.class.name
      },
      subject: %{
        id: classplan.class_subject.subject.id,
        name: classplan.class_subject.subject.name
      },
      content_topics: map_content_topics(classplan.content_topics),
      begin_date: classplan.begin_date,
      end_date: classplan.end_date,
      class_subject_id: classplan.class_subject_id,
      attachment: classplan.attachment,
      url: Api.Attachment.url({classplan.attachment, classplan})}
  end

  def render("months_total_by_year.json", %{data: data}) do
    %{data: data}
  end


  defp map_content_topics(topics) do
    Enum.map topics, fn t -> %{id: t.id, text: t.text} end
  end

end
