defmodule Api.UserView do
  use Api.Web, :view

  alias Api.{UserView}

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user, token: token}) do
    %{data: render_one(user, UserView, "user_token.json", token: token)}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      email: user.email,
      username: user.username,
      admin: user.role.admin,
      role: user.role.name}
  end

  def render("user_token.json", %{user: user, token: token}) do
    %{id: user.id,
      email: user.email,
      token: token,
      username: user.username,
      admin: user.role.admin,
      role: user.role.name}
  end
end
