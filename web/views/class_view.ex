defmodule Api.ClassView do
  use Api.Web, :view

  def render("index.json", %{classes: classes}) do
    %{data: render_many(classes, Api.ClassView, "class.json")}
  end

  def render("index_with_classplans_total.json", %{classes: classes}) do
    %{data: render_many(classes, Api.ClassView, "class_classplans_total.json")}
  end

  def render("show.json", %{class: class}) do
    %{data: render_one(class, Api.ClassView, "class.json")}
  end

  def render("show_with_associations.json", %{class: class}) do
    %{data: render_one(class, Api.ClassView, "class_students_professors.json")}
  end

  def render("show_with_professors.json", %{class: class}) do
    %{data: render_one(class, Api.ClassView, "class_professors.json")}
  end

  def render("class.json", %{class: class}) do
    %{id: class.id,
      name: class.name}
  end

  def render("class_students.json", %{class: class}) do
    %{id: class.id,
      name: class.name,
      classroom_id: class.classroom_id,
      students: handle_students(class.students)
     }
  end

  def render("class_professors.json", %{class: class}) do
    %{id: class.id,
      name: class.name,
      classroom_id: class.classroom_id,
      professors: handle_professors(class.professors)
    }
  end

  def render("class_students_professors.json", %{class: class}) do
    %{id: class.id,
      name: class.name,
      classroom: (if class.classroom, do: handle_classroom( class.classroom )),
      students: handle_students(class.students),
      professors: handle_professors(class.professors)
    }
  end

  def render("class_classplans_total.json", %{class: class}) do
    %{id: class.id,
      name: class.name,
      classplans_total: class.total}
  end


  def render("search.json", %{classes: classes}) do
    %{data: render_many(classes, Api.ClassView, "class.json")}
  end

  def render("search_name.json", %{classes: classes}) do
    %{data: render_many(classes, Api.ClassView, "class_only_name.json")}
  end

  def render("classroom_only_name.json", %{classroom: classroom}) do
    %{name: classroom.name}
  end

  def render("count.json", %{count: count}) do
    %{count: count}
  end

  # Deletes from association 'responsibles' the __meta__ and __struct__ fields together
  # along removes the students field association as well. So that we don't have encoder
  # errors when transform it to JSON.
  defp handle_students(students) do
    Enum.map(students, fn(student) ->
      student
      |> Map.delete(:__meta__)
      |> Map.from_struct
      |> Enum.reject(fn {k, _} -> k == :class end)
      |> Enum.reject(fn {k, _} -> k == :responsibles end)
      |> Enum.into(%{})
      |> Map.take([:id, :name])
    end)
  end


  defp handle_professors(professors) do
    Enum.map(professors, fn(professor) ->
      professor
      |> Map.delete(:__meta__)
      |> Map.from_struct
      |> Enum.reject(fn {k, _} -> k == :classes end)
      |> Enum.into(%{})
      |> Map.take([:id, :name])
    end)
  end

  defp handle_classroom(classroom) do
    classroom
    |> Map.take([:id, :name])
  end

  def render("show_professors.json", %{professors: professors}) do
    %{data: render_many(professors, Api.ProfessorView, "professor_name.json")}
  end

  def render("show_subjects.json", %{subjects: subjects}) do
    %{data: render_many(subjects, Api.SubjectView, "subject_name.json")}
  end

  def render("show_subject_and_professor.json", %{class_subjects: class_subjects}) do
    %{data: render_many(class_subjects, Api.ClassSubjectView, "subjects_professors.json")}
  end

end
