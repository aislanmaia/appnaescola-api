defmodule Api.ActivityView do
  use Api.Web, :view

  def render("index.json", %{activities: activities}) do
    %{data: render_many(activities, Api.ActivityView, "activity.json")}
  end

  def render("show.json", %{activity: activity}) do
    %{data: render_one(activity, Api.ActivityView, "activity.json")}
  end

  def render("activity.json", %{activity: activity}) do
    %{id: activity.id,
      name: activity.name,
      description: activity.description,
      due_date: activity.due_date,
      file: activity.file,
      url: Api.File.url({activity.file, activity})}
  end
end
