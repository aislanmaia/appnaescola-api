defmodule Api.StudentView do
  use Api.Web, :view

  def render("index.json", %{students: students}) do
    %{data: render_many(students, Api.StudentView, "student_name.json")}
  end

  def render("show.json", %{student: student}) do
    %{data: render_one(student, Api.StudentView, "student.json")}
  end

  def render("show_with_responsibles.json", %{student: student}) do
    %{data: render_one(student, Api.StudentView, "student_responsibles.json")}
  end

  def render("search.json", %{students: students}) do
    %{data: render_many(students, Api.StudentView, "student_name.json")}
  end

  def render("student.json", %{student: student}) do
    %{id: student.id,
      name: student.name,
      birthdate: student.birthdate,
      phone: student.phone,
      email: student.email,
      sex: student.sex
      }
  end

  def render("student_name.json", %{student: student}) do
    %{id: student.id,
      name: student.name}
  end


  def render("student_responsibles.json", %{student: student}) do
    %{id: student.id,
      name: student.name,
      birthdate: student.birthdate,
      phone: student.phone,
      email: student.email,
      sex: student.sex,
      class: handle_class(student.class),
      responsibles: handle_responsibles(student.responsibles)}
  end

  # Deletes from association 'responsibles' the __meta__ and __struct__ fields together
  # along removes the students field association as well. So that we don't have encoder
  # errors when transform it to JSON.
  defp handle_responsibles(responsibles) do
    Enum.map(responsibles, fn(resp) ->
      resp
      |> Map.delete(:__meta__)
      |> Map.from_struct
      |> Enum.reject(fn {k, _} -> k == :students end)
      |> Enum.into(%{})
    end)
  end

  defp handle_class(class) do
    if class, do: Map.take(class, [:id, :name]), else: ""
  end

end
